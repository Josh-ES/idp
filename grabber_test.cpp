#include <iostream>
#include <robot_instr.h>
#include <robot_link.h>
#include <delay.h>

using namespace std;

// The id number (see below)
#define ROBOT_NUM 17

// datatype for the robot link
robot_link rlink;

int main ()
{
  // data from microprocessor
  int val;

  #ifdef __arm__
    // setup to run locally on the robot's hardware
    if (!rlink.initialise()) {
  #else
    // setup to run over a link
    if (!rlink.initialise(ROBOT_NUM)) {
  #endif
    cout << "Cannot initialise link" << endl;
    rlink.print_errs("    ");
    return -1;
  }

  // send test instruction
  val = rlink.request (TEST_INSTRUCTION);

  // check result
  if (val == TEST_INSTRUCTION_RESULT) {
    cout << "Test passed" << endl;

  int v = rlink.request(READ_PORT_5);

    // flash the pin number 5 on and off
    for (int i = 0; i < 20; i++) {
      if (
      if (v & (1 << 5)) {
	rlink.command(WRITE_PORT_5, (v & 239));
	cout << "actuator retracted" << endl; 
      else	
	rlink.command(WRITE_PORT_5, (v | (1 << 5));
	cout << "actuator extended" << endl;
      }
      delay(1000);
    }

    // all OK, finish
    return 0;
  }
  else if (val == REQUEST_ERROR) {
    cout << "Fatal errors on link:" << endl;
    rlink.print_errs();
  }
  else {
    cout << "Test failed (bad value returned)" << endl;
  }

  // error, finish
  return -1;
}
