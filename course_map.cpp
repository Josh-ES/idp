#include <iostream>
using namespace std;

// import internal header files
#include "headers/course_map.h"

int main() {
  Node start("S", S);
  Node pickUp("P", P, 90);
  Node hotDropOff("DH", DH, 90);
  Node alpha("A");
  Node beta("B");
  Node charlie("C");
  Node flashingDropOff("DF", DF, 270);
  Node dropOff3("D3", D3, 0);
  Node dropOff2("D2", D2, 0);
  Node dropOff1("D1", D1, 0);
}
