#ifndef NAVIGABLE_DEF
  #define NAVIGABLE_DEF

  #include <bitset>
  #include <delay.h>
  #include <stdlib.h>
  #include <stopwatch.h>
  #include <limits>
  using namespace std;

  #include "edge.h"
  #include "landmark.h"
  #include "motor.h"
  #include "node.h"
  #include "ir_ports.h"
  #include "route_data.h"

  float getSensorVoltage(int sensorReading) {
    float supplyVoltage = 5.0;
    return (sensorReading / 255.0) * supplyVoltage;
  }

  float getDistance(float sensorVoltage) {
    // find the distance relayed by the sensor in mm
    return sensorVoltage * 50.0 / 3.1;
  }

  stopwatch watch;

  class Navigable {
    protected:
      // internal props
      bool currentlyNavigating;
      Motor actuator;
      Node currentLocation;
      int currentDirection;
      Node destination;

      // internal helper methods
      void followLine(bool reverse = false);
      void navigateAlong(Edge edgeToTraverse);
      void failure(bool reverse = false);

    public:
      void turn(int turnAngle);
      Navigable(Motor motor, Node location = start, int initialAngle = 90, Node endpoint = pickUp);

      // public general purpose methods
      void setDestination(Node nextDestination);
      void lightTest();
      void navigate();
      void align();
      void backOut();
  };

  Navigable::Navigable(Motor motor, Node location, int initialAngle, Node endpoint) :
    actuator(motor),
    currentLocation(location),
    currentDirection(initialAngle),
    destination(endpoint)
  {
    rlink.command(WRITE_PORT_5, 0x07);
  }

  void Navigable::setDestination(Node nextDestination) {
    destination = nextDestination;
  }

  void Navigable::turn(int angle) {
    int turnCount = 0;
    bool rearOnWhite = true;
    stopwatch watch;

    int steps = angle / 90;

    while (true) {
      // get data from all the sensors
      int photodiodes = rlink.request(READ_PORT_5);

      if (steps > 0) {
        actuator.turnCounterClockwise();
      } else if (steps < 0) {
        actuator.turnClockwise();
      } else {
        break;
      }

      // get individual sensor values
      int rear = (photodiodes & (1 << rearSensorPort)) >> rearSensorPort;

      // note that on white is one, on black is a zero
      // white: HIGH, black: LOW

      // if the right and center light positions on white, turn right
      if (rear && !rearOnWhite) {
        rearOnWhite = true;
        turnCount++;
      }

      if (!rear) {
        rearOnWhite = false;
      }

      if (turnCount == abs(steps)) {
        actuator.allStop();
        break;
      }
    }
  }

  void Navigable::failure(bool reverse) {
    bool recovered = false;
    stopwatch watch;

    // start a stopwatch, which we'll use in recovery
    watch.start();

    while (!recovered) {
      // get data from all the sensors
      int photodiodes = rlink.request(READ_PORT_5);

      // get individual sensor values
      int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;

      if (reverse) {
        if ((watch.read() / 1000) % 2 == 0) {
          actuator.reverseRightTurn();
        } else {
          actuator.reverseLeftTurn();
        }
      } else {
        if ((watch.read() / 1000) % 2 == 0) {
          actuator.rightTurn();
        } else {
          actuator.leftTurn();
        }
      }

      if (forwardCenter) {
        recovered = true;
      }
    }

    watch.stop();
  }

  void Navigable::followLine(bool reverse) {
    int turnCount = 0;

    bool lockBreakpoints = false;

    // get data from all the sensors
    int photodiodes = rlink.request(READ_PORT_5);

    // get individual sensor values
    int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
    int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
    int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

    // if the robot starts with all forward sensors on white, then lock breakpoints
    if (forwardRight && forwardCenter && forwardLeft) {
      lockBreakpoints = true;
    }

    while (true) {
      // get data from all the sensors
      int photodiodes = rlink.request(READ_PORT_5);

      // get individual sensor values
      int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
      int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
      int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

      // note that on white is one, on black is a zero
      // white: HIGH, black: LOW
      if (!forwardLeft || !forwardCenter || !forwardRight) {
        lockBreakpoints = false;
      }

      if (reverse) {
        // if the right and center light positions on white, turn right
        if (forwardRight && !forwardLeft) {
          actuator.reverseLeftTurn();
        }

        // if the left and center light positions on white, turn left
        else if (forwardLeft && !forwardRight) {
          actuator.reverseRightTurn();
        }

        // if center light on white, left and right on black, move forwards
        else if (
          (forwardCenter && !forwardLeft && !forwardRight) ||
          (forwardCenter && forwardLeft && forwardRight)
        ) {
          actuator.moveBackwards();
        }

        else {
          failure();
        }
      } else {
        // if the right and center light positions on white, turn right
        if (forwardRight && !forwardLeft) {
          actuator.rightTurn();
        }

        // if the left and center light positions on white, turn left
        else if (forwardLeft && !forwardRight) {
          actuator.leftTurn();
        }

        // if center light on white, left and right on black, move forwards
        else if (
          (forwardCenter && !forwardLeft && !forwardRight) ||
          (forwardCenter && forwardLeft && forwardRight)
        ) {
          actuator.moveForwards();
        }

        else {
          failure();
        }
      }

      if (forwardCenter && forwardLeft && forwardRight && !lockBreakpoints) {
        turnCount++;
      }

      if (turnCount > 1) {
        cout << "Node" << endl;
        break;
      }
    }
  }

  void Navigable::navigateAlong(Edge edgeToTraverse) {
    Drive drive = edgeToTraverse.getForwardsDrive();
    int direction = edgeToTraverse.getDirection();

    if (drive == REVERSE) {
      direction -= 180;
    }

    int angle1 = currentDirection - direction;
    int angle2 = - 360 + angle1;

    if (abs(angle1) > abs(angle2)) {
      turn(angle2);
    } else {
      turn(angle1);
    }

    currentDirection = direction;

    followLine(drive == REVERSE);
  }

  void Navigable::lightTest() {
    while (true) {
      // get data from all the sensors
      int photodiodes = rlink.request(READ_PORT_5);

      // get individual sensor values
      int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
      int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
      int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;
      int rear = (photodiodes & (1 << rearSensorPort)) >> rearSensorPort;

      cout << forwardLeft << " " << forwardCenter << " " << forwardRight << endl;
      cout << rear << endl << endl;
    }
  }

  void Navigable::navigate() {
    if (destination.getId() == currentLocation.getId()) {
      cout << "We're already there" << endl;
    }

    stack<Edge> path = courseMap.getPath(currentLocation, destination);

    while (!path.empty()) {
      // get the front edge from the queue and remove it from the queue
      Edge it = path.top();
      path.pop();
      // cout << it.getStartId() << " " << it.getEndId() << endl;
      navigateAlong(it);
    }

    // the current location is now at the selected destination
    currentLocation = destination;

    actuator.allStop();
    // cout << endl;
  }

  void Navigable::align() {
    int direction = currentLocation.getLandmarkDirection();

    int angle1 = currentDirection - direction;
    int angle2 = - 360 + angle1;

    if (abs(angle1) > abs(angle2)) {
      turn(angle2);
    } else {
      turn(angle1);
    }

    if (currentLocation.getLandmark() == P) {
      watch.start();

      // Just use the line following and impact with the target
      while (true) {
        // get data from all the sensors
        int photodiodes = rlink.request(READ_PORT_5);

        // get individual sensor values
        int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
        int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
        int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

        // note that on white is one, on black is a zero
        // white: HIGH, black: LOW

        // if the right and center light positions on white, turn right
        if (forwardRight && forwardCenter && !forwardLeft) {
          actuator.rightTurnSlowly();
        }

        // if the left and center light positions on white, turn left
        if (forwardLeft && forwardCenter && !forwardRight) {
          actuator.leftTurnSlowly();
        }

        // if center light on white, left and right on black, move forwards
        if (
          (forwardCenter && !forwardLeft && !forwardRight) ||
          (forwardCenter && forwardLeft && forwardRight)
        ) {
          actuator.moveForwardsSlowly();
        }

        int dataBits = rlink.request(READ_PORT_5);

        if (((dataBits & (1 << 6)) >> 6) || (watch.read() / 1000 > 2)) {
          break;
        }
      }

      watch.stop();
    } else {
      float previousLeftDistance = 0;
      float previousRightDistance = 0;

      watch.start();

      // Just use the line following and impact with the target
      while (true) {
        // get data from all the sensors
        int photodiodes = rlink.request(READ_PORT_5);

        // get individual sensor values
        int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
        int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
        int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

        // note that on white is one, on black is a zero
        // white: HIGH, black: LOW

        // if the right and center light positions on white, turn right
        if (forwardRight && forwardCenter && !forwardLeft) {
          actuator.rightTurnSlowly();
        }

        // if the left and center light positions on white, turn left
        if (forwardLeft && forwardCenter && !forwardRight) {
          actuator.leftTurnSlowly();
        }

        // if center light on white, left and right on black, move forwards
        if (
          (forwardCenter && !forwardLeft && !forwardRight) ||
          (forwardCenter && forwardLeft && forwardRight)
        ) {
          actuator.moveForwardsSlowly();
        }

        // get data from both of the IR sensors
        int leftSensorValue = rlink.request(ADC1);
        int rightSensorValue = rlink.request(ADC0);

        // get the sensor voltages from the ADC values
        float leftSensorVoltage = getSensorVoltage(leftSensorValue);
        float rightSensorVoltage = getSensorVoltage(rightSensorValue);

        // get the distances from the two sensor voltages
        float leftDistance = getDistance(leftSensorVoltage);
        float rightDistance = getDistance(rightSensorVoltage);

        if ((leftDistance < previousLeftDistance && rightDistance < previousRightDistance) || (watch.read() / 1000 > 4)) {
          break;
        }

        previousLeftDistance = leftDistance;
        previousRightDistance = rightDistance;
      }

      // Use IR sensors' differential distance prototype
      while (true) {
        // get data from both of the IR sensors
        int leftSensorValue = rlink.request(ADC1);
        int rightSensorValue = rlink.request(ADC0);

        // get the sensor voltages from the ADC values
        float leftSensorVoltage = getSensorVoltage(leftSensorValue);
        float rightSensorVoltage = getSensorVoltage(rightSensorValue);

        // get the distances from the two sensor voltages
        float leftDistance = getDistance(leftSensorVoltage);
        float rightDistance = getDistance(rightSensorVoltage);

        // get the error and define proportional control variables
        float Kp = 2.0;
        float error = leftDistance - rightDistance;

        // get the two speeds
        float differentialSpeed = Kp * error;
        float leftSpeed = motor2LowSpeed + differentialSpeed;
        float rightSpeed = motor1LowSpeed - differentialSpeed;

        actuator.drive(leftSpeed, rightSpeed);

        // check the microswitch for a press, which signals contact with the white box
        int dataBits = rlink.request(READ_PORT_5);

        if (((dataBits & (1 << 6)) >> 6) || (watch.read() / 1000 > 4)) {
          actuator.allStop();
          break;
        }
      }

      watch.stop();
    }
  }

  void Navigable::backOut() {
    int turnCount = 0;

    bool lockBreakpoints = false;

    stopwatch watch;

    if (currentLocation.getLandmark() == P) {
      while (true) {
        // get data from all the sensors
        int photodiodes = rlink.request(READ_PORT_5);

        // get individual sensor values
        int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
        int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
        int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

        // note that on white is one, on black is a zero
        // white: HIGH, black: LOW
        if (!forwardLeft || !forwardCenter || !forwardRight) {
          lockBreakpoints = false;
        }

        actuator.moveBackwards();

        if (forwardCenter && forwardLeft && forwardRight && !lockBreakpoints) {
          turnCount++;
        }

        if (turnCount > 1) {
          cout << "Node" << endl;
          break;
        }
      }
    } else {
      while (true) {
        // get data from both of the IR sensors
        int leftSensorValue = rlink.request(ADC1);
        int rightSensorValue = rlink.request(ADC0);

        // get the sensor voltages from the ADC values
        float leftSensorVoltage = getSensorVoltage(leftSensorValue);
        float rightSensorVoltage = getSensorVoltage(rightSensorValue);

        // get the distances from the two sensor voltages
        float leftDistance = getDistance(leftSensorVoltage);
        float rightDistance = getDistance(rightSensorVoltage);

        // get the error and define proportional control variables
        float Kp = 2.0;
        float error = leftDistance - rightDistance;

        // get the two speeds
        float differentialSpeed = Kp * error;
        float leftSpeed = motor2LowSpeedReverse - differentialSpeed;
        float rightSpeed = motor1LowSpeedReverse + differentialSpeed;

        actuator.drive(leftSpeed, rightSpeed);

        // get data from all the sensors
        int photodiodes = rlink.request(READ_PORT_5);

        // get individual sensor values
        int forwardRight = (photodiodes & (1 << rightSensorPort)) >> rightSensorPort;
        int forwardCenter = (photodiodes & (1 << centerSensorPort)) >> centerSensorPort;
        int forwardLeft = (photodiodes & (1 << leftSensorPort)) >> leftSensorPort;

        if (forwardLeft && forwardCenter && forwardRight) {
          actuator.allStop();
          break;
        }
      }
    }
  }
#endif
