#ifndef COURSE_MAP_DEF
  #define COURSE_MAP_DEF

  #include <vector>
  #include <queue>
  #include <stack>
  #include <iostream>
  #include <limits>
  using namespace std;

  // include internal header files
  #include "route_data.h"
  #include "edge.h"

  class CourseMap {
    private:
      bool reverse;
      vector<Edge> edges;
      vector<Edge> edgesBackup;

    public:
      CourseMap(vector<Edge> courseEdges);

      // public utility methods
      vector<Edge> getEdgesFromPoint(Node point);
      stack<Edge> getPath(Node beginning, Node end);
      Edge& getEdge(int id);
  };

  CourseMap::CourseMap(vector<Edge> courseEdges) {
    edges.reserve(10);
    reverse = false;
    edges = courseEdges;
    edgesBackup = courseEdges;
  }

  vector<Edge> CourseMap::getEdgesFromPoint(Node point) {
    vector<Edge> edgesFromPoint;

    for (vector<Edge>::iterator it = edges.begin(); it != edges.end(); ++it) {
      if ((it->getStartId() == point.getId()) || (it->getEndId() == point.getId())) {
        Edge& edge = getEdge(it->getId());
        edgesFromPoint.push_back(edge);
      }
    }

    return edgesFromPoint;
  }

  stack<Edge> CourseMap::getPath(Node beginning, Node end) {
    // we need a queue for a breadth-first search, as we need the first in to be the first out
    queue<Edge> Q;
    // the path on the other hand must be a stack, as we need the last element into the list
    // to be the first one out
    stack<Edge> path;

    vector<Edge> edgesFromPoint = getEdgesFromPoint(beginning);

    for (vector<Edge>::iterator it = edgesFromPoint.begin(); it != edgesFromPoint.end(); ++it) {
      Edge& edge = getEdge(it->getId());

      if (!edge.getVisited()) {
        edge.setVisited(true);
        edge.setDistance(1);

        if (edge.getEndId() == end.getId() || edge.getStartId() == end.getId()) {
          // empty the stack and add the chosen edge to it
          path.empty();

          // ensure the top edge in the path stack has its endpoint at the desired end of the route
          if (edge.getEndId() != end.getId()) {
            edge.reverse();
          }

          path.push(edge);
          return path;
        } else {
          Q.push(edge);
        }
      }
    }

    // effectively sets these integers to infinity - the maximum possible integer that can be stored
    int turnsRequired = numeric_limits<int>::max();
    int distanceRequired = numeric_limits<int>::max();

    while (!Q.empty()) {
      Edge e = Q.front();
      Q.pop();

      if (e.getDistance() > distanceRequired) {
        break;
      }

      vector<Edge> edgesFromPoint;

      // look for whether the end of the selected edge has already been visited
      if (e.getPrevious() >= 0) {
        Edge previous = getEdge(e.getPrevious());

        if (
          (previous.getEndId() == e.getStartId()) ||
          (previous.getStartId() == e.getStartId())
        ) {
          edgesFromPoint = getEdgesFromPoint(e.getEnd());
        } else {
          edgesFromPoint = getEdgesFromPoint(e.getStart());
        }
      } else {
        if (beginning.getId() == e.getStartId()) {
          edgesFromPoint = getEdgesFromPoint(e.getEnd());
        } else {
          edgesFromPoint = getEdgesFromPoint(e.getStart());
        }
      }

      for (vector<Edge>::iterator it = edgesFromPoint.begin(); it != edgesFromPoint.end(); ++it) {
        if (it->getId() != e.getId()) {
          Edge& edge = getEdge(it->getId());

          if (!edge.getVisited() || (edge.getDistance() > e.getDistance() + 1) || (edge.getTurns() > e.getTurns())) {
            edge.setVisited(true);
            edge.setDistance(e.getDistance() + 1);

            if (edge.getDirection() != e.getDirection()) {
              edge.setTurns(e.getTurns() + 1);
            } else {
              edge.setTurns(e.getTurns());
            }

            edge.setPrevious(e.getId());

            if ((edge.getEndId() == end.getId() || edge.getStartId() == end.getId()) && edge.getTurns() < turnsRequired) {
              // empty the stack...
              stack<Edge> empty;
              swap(path, empty);

              // ensure the top edge in the path stack has its endpoint at the desired end of the route
              if (edge.getEndId() != end.getId()) {
                edge.reverse();
              }

              // ...and add the chosen edge to it
              path.push(edge);

              turnsRequired = edge.getTurns();
              distanceRequired = edge.getDistance();
            } else {
              Q.push(edge);
            }
          }
        }
      }
    }

    int previousDistance = numeric_limits<int>::max();

    // loop back through the path from the end node, adding each edge to the path stack
    while (previousDistance != 1) {
      Edge edge = path.top();

      // get the previous edge using the pointer built into the edge object
      Edge previous = getEdge(edge.getPrevious());

      // make sure the edges link together, reversing the edge so the end of one edge leads
      // into the start of the next
      if (edge.getStartId() != previous.getEndId()) {
        previous.reverse();
      }

      path.push(previous);
      previousDistance = previous.getDistance();
    }

    edges = edgesBackup;
    return path;
  }

  Edge& CourseMap::getEdge(int id) {
    return edges.at(id);
  }
#endif
