#ifndef MOTOR_DEF
  #define MOTOR_DEF

  #include "robot_link.h"
  #include "motor_speeds.h"

  class Motor {
    protected:
      int leftMotorPort;
      int rightMotorPort;

    public:
      Motor(int leftMotorPortNumber, int rightMotorPortNumber);

      // general purpose virtual methods for extension
      void moveForwards();
      void moveBackwards();
      void moveForwardsSlowly();
      void leftTurn();
      void leftTurnSlowly();
      void rightTurnSlowly();
      void rightTurn();
      void reverseLeftTurn();
      void reverseRightTurn();
      void turnClockwise();
      void turnCounterClockwise();
      void allStop();
      void drive(int leftSpeed, int rightSpeed);
  };

  Motor::Motor(int leftMotorPortNumber, int rightMotorPortNumber) {
    leftMotorPort = leftMotorPortNumber;
    rightMotorPort = rightMotorPortNumber;
  }

  void Motor::moveForwards() {
    rlink.command(MOTOR_1_GO, motor1FullSpeed);
    rlink.command(MOTOR_2_GO, motor2FullSpeed);
  }

  // line follow slowly
  void Motor::moveForwardsSlowly() {
    rlink.command(MOTOR_1_GO, motor1LowSpeed);
    rlink.command(MOTOR_2_GO, motor2LowSpeed);
  }

  void Motor::moveBackwards() {
    rlink.command(MOTOR_1_GO, motor1FullSpeedReverse);
    rlink.command(MOTOR_2_GO, motor2FullSpeedReverse);
  }

  // Line following correction
  void Motor::rightTurn() {
    rlink.command(MOTOR_1_GO, motor1ReducedSpeed);
    rlink.command(MOTOR_2_GO, motor2FullSpeed);
  }

  // Line following correction
  void Motor::leftTurn() {
    rlink.command(MOTOR_1_GO, motor1FullSpeed);
    rlink.command(MOTOR_2_GO, motor2ReducedSpeed);
  }

  // Slow line following correction
  void Motor::rightTurnSlowly() {
    rlink.command(MOTOR_1_GO, motor1LowReducedSpeed);
    rlink.command(MOTOR_2_GO, motor2LowSpeed);
  }

  // Slow line following correction
  void Motor::leftTurnSlowly() {
    rlink.command(MOTOR_1_GO, motor1LowSpeed);
    rlink.command(MOTOR_2_GO, motor2LowReducedSpeed);
  }
  // Reverse line following correction
  void Motor::reverseRightTurn() {
    rlink.command(MOTOR_1_GO, motor1ReducedSpeedReverse);
    rlink.command(MOTOR_2_GO, motor2FullSpeedReverse);
  }

  // Reverse line following correction
  void Motor::reverseLeftTurn() {
    rlink.command(MOTOR_1_GO, motor1FullSpeedReverse);
    rlink.command(MOTOR_2_GO, motor2ReducedSpeedReverse);
  }

  // Turn on the spot
  void Motor::turnClockwise() {
    rlink.command(MOTOR_1_GO, motor1RightTurn);
    rlink.command(MOTOR_2_GO, motor2RightTurn);
  }

  // Turn on the spot
  void Motor::turnCounterClockwise() {
    rlink.command(MOTOR_1_GO, motor1LeftTurn);
    rlink.command(MOTOR_2_GO, motor2LeftTurn);
  }

  void Motor::allStop() {
    rlink.command(MOTOR_1_GO, motorStop);
    rlink.command(MOTOR_2_GO, motorStop);
  }

  void Motor::drive(int leftSpeed, int rightSpeed) {
    rlink.command(MOTOR_1_GO, rightSpeed);
    rlink.command(MOTOR_2_GO, leftSpeed);
  }
#endif
