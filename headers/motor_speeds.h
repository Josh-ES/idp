#ifndef MOTOR_SPEEDS_DEF
  #define MOTOR_SPEEDS_DEF

  // Motor 1 is the right hand one, Motor 2 is the left hand one

  // CAUTION
  //
  // multiplying a reverse speed (i.e. a speed of "128 + forwardsSpeed") by
  // a reducing factor, here 1.3, will often result in an unintended effect.
  // Be careful of considering "1.3 * (128 + forwardsSpeed)" as equivalent to
  // "128 + 1.3 * forwardsSpeed".

  // define robot number
  #define ROBOT_NUM  17

  // define the margin to reduce speeds by
  float reduce = 1.2;

  // define forward motor speeds
  int motor1FullSpeed = 100;
  int motor1ReducedSpeed = (motor1FullSpeed) / reduce;
  int motor1LowSpeed = 50;
  int motor1LowReducedSpeed = (motor1LowSpeed) / reduce;

  int motor2FullSpeed = 128 + 107;
  int motor2ReducedSpeed = 128 + (motor2FullSpeed - 128) / reduce;
  int motor2LowSpeed = 128 + 49;
  int motor2LowReducedSpeed = 128 + (motor2LowSpeed - 128) / reduce;

  int motorStop = 0;

  //define reverse motor speeds
  int motor1FullSpeedReverse = 128 + motor1FullSpeed;
  int motor1ReducedSpeedReverse = 128 + motor1FullSpeedReverse / reduce;
  int motor1LowSpeedReverse = 128 + motor1LowSpeed;

  int motor2FullSpeedReverse = -128 + motor2FullSpeed;
  int motor2ReducedSpeedReverse = -128 + motor2FullSpeedReverse / reduce;
  int motor2LowSpeedReverse = -128 + motor2LowSpeed;

  //define turning motor speeds
  int motor1RightTurn = 128 + 45;
  int motor2RightTurn = 128 + 70;

  int motor1LeftTurn = 70;
  int motor2LeftTurn = 45;
#endif
