#ifndef EDGE_DEF
  #define EDGE_DEF

  #include <string>
  #include <limits>
  using namespace std;

  // include internal header files
  #include "node.h"
  #include "drive.h"

  class Edge {
    private:
      // internal class props
      int id;
      Node nodeA;
      Node nodeB;
      Drive forwardsDrive;
      Drive reverseDrive;
      int edgeDirection;
      int biasDirection;

      // private props for route generation
      int distance;
      int turns;
      bool visited;
      int previous;

    public:
      Edge(Node startNode, Node endNode, int direction, Drive forwardsDirection = FORWARDS, Drive reverseDirection = FORWARDS, int bias = 0);

      void reverse();

      // get prop methods
      int getId();
      string getStartId();
      string getEndId();
      Node getStart();
      Node getEnd();
      int getDirection();
      Drive getForwardsDrive();
      Drive getReverseDrive();
      int getBiasDirection();
      int getDistance();
      bool getVisited();
      int getPrevious();
      int getTurns();

      // set prop methods
      void setDistance(int newDistance);
      void setVisited(bool newVisited);
      void setPrevious(int newPrevious);
      void setTurns(int newTurns);
  };

  Edge::Edge(
    Node startNode,
    Node endNode,
    // direction is 90 for horizontal, 0 for vertical
    int direction,
    Drive forwardsDirection,
    Drive reverseDirection,
    int bias
  ) :
    nodeA(startNode),
    nodeB(endNode),
    forwardsDrive(forwardsDirection),
    reverseDrive(reverseDirection),
    edgeDirection(direction),
    biasDirection(bias)
  {
    static int idHolder = 0;
    id = idHolder;
    idHolder++;

    previous = numeric_limits<int>::min();
  }

  void Edge::reverse() {
    edgeDirection = (180 + edgeDirection) % 360;

    Node temp = nodeA;
    nodeA = nodeB;
    nodeB = temp;

    Drive tempDrive = forwardsDrive;
    forwardsDrive = reverseDrive;
    reverseDrive = tempDrive;
  }

  int Edge::getId() {
    return id;
  }

  string Edge::getStartId() {
    return nodeA.getId();
  }

  string Edge::getEndId() {
    return nodeB.getId();
  }

  Node Edge::getStart() {
    return nodeA;
  }

  Node Edge::getEnd() {
    return nodeB;
  }

  Drive Edge::getForwardsDrive() {
    return forwardsDrive;
  }

  Drive Edge::getReverseDrive() {
    return reverseDrive;
  }

  int Edge::getDirection() {
    return edgeDirection;
  }

  int Edge::getBiasDirection() {
    return biasDirection;
  }

  int Edge::getDistance() {
    return distance;
  }

  bool Edge::getVisited() {
    return visited;
  }

  int Edge::getPrevious() {
    return previous;
  }

  int Edge::getTurns() {
    return turns;
  }

  void Edge::setDistance(int newDistance) {
    distance = newDistance;
  }

  void Edge::setVisited(bool newVisited) {
    visited = newVisited;
  }

  void Edge::setPrevious(int newPrevious) {
    previous = newPrevious;
  }

  void Edge::setTurns(int newTurns) {
    turns = newTurns;
  }
#endif
