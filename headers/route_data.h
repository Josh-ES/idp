#ifndef ROUTE_DATA_DEF
  #define ROUTE_DATA_DEF

  #include <vector>
  #include <iostream>
  using namespace std;

  // include internal header files
  #include "course_map.h"

  Node start("S", S, 0);
  Node start0("SN");
  Node start90("SE");
  Node start180("SS");
  Node start270("SW");
  Node pickUp("P", P, 90);
  Node hotDropOff("DH", DH, 90);
  Node alpha("A"); // node halfway across table, by red obstacle
  Node bravo("B"); // node on far left of table, inline with red obstacle
  Node charlie("C"); // node on far left of table, just before DF drop off point
  Node delta("D"); // node in the middle of the table, just above the red obstacle
  Node echo("E"); // node in the bottom left corner of the table
  Node flashingDropOff("DF", DF, 270);
  Node dropOff3("D3", D3, 0);
  Node dropOff2("D2", D2, 0);
  Node dropOff1("D1", D1, 0);

  Edge edgeArray[17] = {
    Edge(start, start0, 0),
    Edge(start, start90, 90),
    Edge(start, start180, 180),
    Edge(start, start270, 270),
    Edge(start90, pickUp, 90),
    Edge(start270, echo, 270),
    Edge(echo, bravo, 0),
    Edge(pickUp, hotDropOff, 0),
    Edge(hotDropOff, alpha, 270),
    Edge(alpha, bravo, 270),
    Edge(bravo, charlie, 0),
    Edge(charlie, delta, 90),
    Edge(delta, alpha, 180),
    Edge(charlie, flashingDropOff, 0),
    Edge(flashingDropOff, dropOff3, 0),
    Edge(dropOff3, dropOff2, 90),
    Edge(dropOff2, dropOff1, 90)
  };

  std::vector<Edge> edges(edgeArray, edgeArray + sizeof(edgeArray) / sizeof(edgeArray[0]));

  CourseMap courseMap(edges);
#endif
