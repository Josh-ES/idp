#ifndef NODE_DEF
  #define NODE_DEF

  #include <string>
  using namespace std;

  // include internal header files
  #include "landmark.h"

  class Node {
    private:
      string id;
      Landmark landmark;
      int landmarkDirection;

    public:
      Node(string identifier, Landmark landmarkAtNode = none, int landmarkDirectionAtNode = -1);

      // get prop methods
      string getId();
      Landmark getLandmark();
      int getLandmarkDirection();
  };

  Node::Node(
    string identifier,
    Landmark landmarkAtNode,
    int landmarkDirectionAtNode
  ) :
    id(identifier),
    landmark(landmarkAtNode),
    landmarkDirection(landmarkDirectionAtNode)
  {

  }

  string Node::getId() {
    return id;
  }

  Landmark Node::getLandmark() {
    return landmark;
  }

  int Node::getLandmarkDirection() {
    return landmarkDirection;
  }
#endif
