#ifndef GRABBER_DEF
  #define GRABBER_DEF

  #include "casting.h"
  #include "motor_speeds.h"
  #include <delay.h>
  #include <vector>
  #include <bitset>

  class Grabber {
    private:
      // internal class properties
      Casting casting;
      bool extended;
      bool closed;
      int angle;
      vector<Casting> deposited;

      // internal helper methods
      void rotateStep(bool clockwise = true);

    public:
      Grabber();

      // get prop methods
      Casting getCasting();
      bool getExtendedState();
      int getAngle();

      // set prop methods
      void setCasting();
      void setExtendedState();
      void setClosedState();
      void setAngle();

      // general purpose methods
      void extendArm();
      void retractArm();
      void rotate(int angle);
  };

  Grabber::Grabber() {
    extended = false;
    closed = false;
    angle = 0;
  }

  Casting Grabber::getCasting() {
    return casting;
  }

  bool Grabber::getExtendedState() {
    int v = rlink.request(READ_PORT_5);

    if ((v & (1 << 5)) >> 5) {
      return extended;
    }

    return false;
  }

  int Grabber::getAngle() {
    return angle;
  }

  void Grabber::extendArm() {
    int v = rlink.request(READ_PORT_5);
    v = (v | (1 << 5));

    if (!getExtendedState()) {
      extended = true;
      rlink.command(WRITE_PORT_5, v);
      delay(1000);
    }

    int tooHot = 100;
    int val = rlink.request(ADC2);

    if (val < tooHot) {
      rlink.command(WRITE_PORT_5, v & 239);
    } else {
      rlink.command(WRITE_PORT_5, v | (1 << 4));
    }

    cout << (rlink.request(ADC3) < 180) << endl;
  }

  void Grabber::retractArm() {
    int v = rlink.request(READ_PORT_5);
    v = (v & 0xDF);

    if (getExtendedState()) {
      extended = false;
      rlink.command(WRITE_PORT_5, v);
      delay(1000);
    }

    int tooHot = 100;
    int val = rlink.request(ADC2);

    if (val < tooHot) {
      rlink.command(WRITE_PORT_5, v & 239);
    } else {
      rlink.command(WRITE_PORT_5, v | (1 << 4));
    }
  }

  void Grabber::rotate(int angle)  {
    rlink.command(MOTOR_4_GO, motor1LowSpeed);
    delay(1000);
    rlink.command(MOTOR_4_GO, 0);
  }
#endif
