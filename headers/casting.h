#ifndef CASTING_DEF
  #define CASTING_DEF

  #include <exception>

  // include internal header files
  #include "robot_link.h"
  #include "route_data.h"
  #include "landmark.h"
  #include "node.h"

  class noAvailableSpace: public exception
  {
    virtual const char* what() const throw()
    {
      return "There are no available destinations for this casting";
    }
  } castingEx;

  class Casting {
    private:
      // internal class props
      int temperature;
      bool flashing;

    public:
      Casting();

      // get prop methods
      int readThermistor();
      bool getTooHotState();
      bool getFlashingState();
      Node getDestination();

      // static tracking variables for keeping track of which drop off points have been used already
      static bool D3Used;
      static bool D2Used;
      static bool D1Used;
      static bool DFUsed;
      static bool DHUsed;
  };

  Casting::Casting() {
    static int id = 0;

    if (id == 1) {
      temperature = 33;
    }

    id++;
  }

  int Casting::readThermistor() {
    // temperature = rlink.request(ADC2);
    return temperature;
  }

  bool Casting::getTooHotState() {
    readThermistor();

    // this is a threshold below which the casting temperature is being read as too hot
    int tooHot = 100;

    // TODO consider the failure state - what happens when the sensor fails and we just get a zero back?
    if (temperature < tooHot) {
      return true;
    } else {
      return false;
    }
  }

  bool Casting::getFlashingState() {
    flashing = rlink.request(ADC3) < 180;
  	return flashing;
  }

  Node Casting::getDestination() {
    if (getFlashingState() && !Casting::DFUsed) {
      Casting::DFUsed = true;
      return flashingDropOff;
    }

    if (getTooHotState() && !Casting::DHUsed) {
      Casting::DHUsed = true;
      return hotDropOff;
    }

    if (!Casting::D3Used) {
      Casting::D3Used = true;
      return dropOff3;
    }

    if (!Casting::D2Used) {
      Casting::D2Used = true;
      return dropOff2;
    }

    if (Casting::D1Used) {
      throw castingEx;
    }

    Casting::D1Used = true;
    return dropOff1;
  }

  bool Casting::D3Used = false;
  bool Casting::D2Used = false;
  bool Casting::D1Used = false;
  bool Casting::DFUsed = false;
  bool Casting::DHUsed = false;
#endif
