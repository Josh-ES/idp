#ifndef LANDMARK_DEF
  #define LANDMARK_DEF

  #include <string>

  enum Landmark { none, D1, D2, D3, S, P, DH, DF };

  string formatLandmarkEnum (Landmark landmark) {
    string landmarkString = "";

    switch (landmark) {
      case 0:
        landmarkString = "none";
        break;

      case 1:
        landmarkString = "D1";
        break;

      case 2:
        landmarkString = "D2";
        break;

      case 3:
        landmarkString = "D3";
        break;

      case 4:
        landmarkString = "S";
        break;

      case 5:
        landmarkString = "P";
        break;

      case 6:
        landmarkString = "DH";
        break;

      case 7:
        landmarkString = "DF";
    };

    return landmarkString;
  }
#endif
