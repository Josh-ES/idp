#ifndef ACTUATOR_DEF
  #define ACTUATOR_DEF

  #include <iostream>
  using namespace std;

  class Actuator {
    public:
      Actuator();

      // general purpose virtual methods for extension
      virtual void moveForwards();
      virtual void moveBackwards();
      virtual void leftTurn();
      virtual void rightTurn();
      virtual void turnClockwise();
      virtual void turnCounterClockwise();
      virtual void allStop();
  };

  Actuator::Actuator() {

  }

  void Actuator::moveForwards() {
    cout << "Move Forwards" << endl;
  }

  void Actuator::moveBackwards() {
    cout << "Move Backwards" << endl;
  }

  void Actuator::rightTurn() {
    cout << "Right Turn" << endl;
  }

  void Actuator::leftTurn() {
    cout << "Left Turn" << endl;
  }

  void Actuator::turnClockwise() {
    cout << "Turn Clockwise" << endl;
  }

  void Actuator::turnCounterClockwise() {
    cout << "Turn Counter Clockwise" << endl;
  }

  void Actuator::allStop() {
    cout << "All Stop" << endl;
  }
#endif
