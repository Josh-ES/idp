// include internal header files
#include "grabber.h"
#include "motor.h"
#include "navigable.h"
#include "course_map.h"

class Robot: public Navigable, public Grabber {
  public:
    Robot(int leftMotorPort, int rightMotorPort, int initialAngle = 90, Node currentLocation = start, Node firstDestination = pickUp);
};

Robot::Robot(int leftMotorPort, int rightMotorPort, int initialAngle, Node currentLocation, Node firstDestination) :
  Navigable(Motor(leftMotorPort, rightMotorPort), currentLocation, initialAngle, firstDestination)
{

}
