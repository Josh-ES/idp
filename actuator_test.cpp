// include internal header files
#include "headers/actuator.h"

int main() {
  Actuator actuator;

  actuator.moveForwards();
  actuator.moveBackwards();
  actuator.turnClockwise();
  actuator.turnCounterClockwise();
  actuator.rightTurn();
  actuator.leftTurn();
  actuator.allStop();

  return 0;
}
