#include <iostream>
#include <string>
#include <delay.h>
using namespace std;

// import internal header files
#include "headers/robot_link.h"
#include "headers/landmark.h"
#include "headers/casting.h"
#include "headers/motor_speeds.h"
#include "headers/robot.h"

int main ()
{
  // set up the motor and move it
  Robot robot(1, 2, 0, alpha);
  robot.align();

  return 0;
}
