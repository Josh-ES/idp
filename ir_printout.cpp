#include <iostream>
#include <string>
#include <delay.h>
using namespace std;

// import internal header files
#include "headers/robot_link.h"
#include "headers/landmark.h"
#include "headers/casting.h"
#include "headers/motor_speeds.h"

float getSensorVoltage(int sensorReading) {
  float supplyVoltage = 5.0;
  return (sensorReading / 255.0) * supplyVoltage;
}

float getLeftDistance(float sensorVoltage) {
  // find the distance relayed by the sensor in mm
  return sensorVoltage * 50.0 / 3.27;
}

float getRightDistance(float sensorVoltage) {
  // find the distance relayed by the sensor in mm
  return sensorVoltage * 50.0 / 3.28;
}

int main ()
{
  // data from microprocessor
  int val;

  #ifdef __arm__
    // setup to run locally on the robot's hardware
    if (!rlink.initialise()) {
  #else
    // setup to run over a link
    if (!rlink.initialise(ROBOT_NUM)) {
  #endif
    cout << "Cannot initialise link" << endl;
    rlink.print_errs("    ");
    return -1;
  }

  // send test instruction
  val = rlink.request (TEST_INSTRUCTION);

  // check result
  if (val == TEST_INSTRUCTION_RESULT) {
    cout << "Test passed" << endl;

    float previousLeftDistance;
    float previousRightDistance;

    float leftDistanceAggregator = 0;
    float rightDistanceAggregator = 0;

    // set up an iterator variable to keep track of the number of loops
    int aggregator = 1;
    // holder variable for the number of data points that are used to calculate an average
    int interval = 25;

    // read the thermistor value every second
    while (true) {
      // get data from both of the IR sensors
      int leftSensorValue = rlink.request(ADC1);
      int rightSensorValue = rlink.request(ADC0);

      // get the sensor voltages from the ADC values
      float leftSensorVoltage = getSensorVoltage(leftSensorValue);
      float rightSensorVoltage = getSensorVoltage(rightSensorValue);

      // get the distances from the two sensor voltages
      leftDistanceAggregator += getLeftDistance(leftSensorVoltage);
      rightDistanceAggregator += getRightDistance(rightSensorVoltage);

      if (aggregator % (interval + 1) == 0) {
        float leftDistance = leftDistanceAggregator / interval;
        float rightDistance = rightDistanceAggregator / interval;

        cout << leftDistance << " " << rightDistance << " " << leftDistance - rightDistance << endl;

        previousLeftDistance = leftDistance;
        previousRightDistance = rightDistance;

        leftDistanceAggregator = 0;
        rightDistanceAggregator = 0;
      }

      aggregator++;

      delay(10);
    }

    // all OK, finish
    return 0;
  }
  else if (val == REQUEST_ERROR) {
    cout << "Fatal errors on link:" << endl;
    rlink.print_errs();
  }
  else {
    cout << "Test failed (bad value returned)" << endl;
  }

  // error, finish
  return -1;
}
