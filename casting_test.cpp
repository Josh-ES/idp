#include <iostream>
#include <string>
#include <delay.h>
using namespace std;

// import internal header files
#include "headers/robot_link.h"
#include "headers/landmark.h"
#include "headers/casting.h"

int main ()
{
  // set up the motor and move it
  Casting casting1;
  Casting casting2;
  Casting casting3;
  Casting casting4;

  cout << formatLandmarkEnum(casting1.getDestination()) << endl;
  cout << formatLandmarkEnum(casting2.getDestination()) << endl;
  cout << formatLandmarkEnum(casting3.getDestination()) << endl;
  cout << formatLandmarkEnum(casting4.getDestination()) << endl;

  return 0;
}
