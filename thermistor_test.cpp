#include <iostream>
#include <robot_instr.h>
#include <robot_link.h>
#include <delay.h>
using namespace std;

// datatype for the robot link
robot_link rlink;
int tooHot = 100;

int main ()
{
  // data from microprocessor
  int val;

  #ifdef __arm__
    // setup to run locally on the robot's hardware
    if (!rlink.initialise()) {
  #else
    // setup to run over a link
    if (!rlink.initialise(ROBOT_NUM)) {
  #endif
    cout << "Cannot initialise link" << endl;
    rlink.print_errs("    ");
    return -1;
  }

  // send test instruction
  val = rlink.request (TEST_INSTRUCTION);

  // check result
  if (val == TEST_INSTRUCTION_RESULT) {
    cout << "Test passed" << endl;

    // read the thermistor value every second
    while (true) {
      int val = rlink.request(ADC2);
      int v = rlink.request(READ_PORT_5);
      cout << val << endl;

      if (val < tooHot) {
        rlink.command(WRITE_PORT_5, v & 239);
      } else {
        rlink.command(WRITE_PORT_5, v | (1 << 4));
      }

      delay(300);
    }

    // all OK, finish
    return 0;
  }
  else if (val == REQUEST_ERROR) {
    cout << "Fatal errors on link:" << endl;
    rlink.print_errs();
  }
  else {
    cout << "Test failed (bad value returned)" << endl;
  }

  // error, finish
  return -1;
}
