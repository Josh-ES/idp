#include <iostream>
#include <delay.h>
#include <stopwatch.h>
using namespace std;

// import internal header files
#include "headers/robot_link.h"
#include "headers/route_data.h"
#include "headers/robot.h"
#include "headers/casting.h"

int main ()
{
  stopwatch watch;

  // data from microprocessor
  int val;

  #ifdef __arm__
    // setup to run locally on the robot's hardware
    if (!rlink.initialise()) {
  #else
    // setup to run over a link
    if (!rlink.initialise(ROBOT_NUM)) {
  #endif
    cout << "Cannot initialise link" << endl;
    rlink.print_errs("    ");
    return -1;
  }

  // send test instruction
  val = rlink.request (TEST_INSTRUCTION);

  // check result
  if (val == TEST_INSTRUCTION_RESULT) {
    cout << "Test passed" << endl;

    // set up the motor and move it
    Robot robot(1, 2, 90, start);

    watch.start();

    for (int i = 0; i < 5; i++) {
      // go to pickUp point
      robot.setDestination(pickUp);
      robot.navigate();

      // align with the pickUp point and pickUp the casting
      robot.align();
      robot.extendArm();
      robot.backOut();

      // get the casting's destination and navigate there
      robot.setDestination(robot.getCasting().getDestination());
      robot.navigate();

      // align with the dropOff point and dropOff the casting
      robot.align();
      robot.retractArm();
      robot.backOut();

      // check for time remaining after depositing the casting
      if (watch.read() / 1000 > 270) {
        break;
      }
    }

    watch.stop();
    robot.setDestination(start);
    robot.navigate();

    // all OK, finish
    return 0;
  }
  else if (val == REQUEST_ERROR) {
    cout << "Fatal errors on link:" << endl;
    rlink.print_errs();
  }
  else {
    cout << "Test failed (bad value returned)" << endl;
  }

  // error, finish
  return -1;
}
