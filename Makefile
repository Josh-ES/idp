# Automatically generated Makefile for arm
navigate.arm : navigate.cpp  headers/robot_link.h headers/route_data.h headers/robot.h
	 arm-linux-gnueabi-g++ -ansi -Wall -g -I/usr/arm-unknown-linux-gnueabi/include -I/export/teach/1BRobot -L/usr/arm-unknown-linux-gnueabi/lib navigate.cpp -o navigate.arm -lrobot
